# Basic ADR application

This applications shows the Action-Domain-Responder pattern in
action.

Tasks:

1. Take some time to familiarise yourself with the application
   then write down a rough outline of how the system works.
2. Add a middleware which caches successful GET requests. If a
   request has already been cached, the cached version should be
   returned.
3. The 'language' entry is currently a free-text input but only
   a limited number of options are available. Can you modify the
   view to show a list of available options? Bonus points if the
   options in the view update when you update the options available
   to the domain. Use any means that you feel are appropriate for
   this task.
4. Add a unit test for
   `\App\Middleware\LanguageToLowerCaseMiddleware` and for your
   new middleware.

# Task 1

I have researched into the system and do have a few questions which would be great to run past you if I were to land the role.

I thought these two resources nicely outline how the system works.

Zend Expressive (https://docs.zendframework.com/zend-expressive/v3/getting-started/features/)

-   The application is a queue, and operates in FIFO (First In First Out) order.
-   Each middleware can choose whether to return a response, which will cause the queue to unwind, or to traverse to the next middleware.
-   Most of the time, you will be defining routed middleware, and the routing rules that map to them.
-   You get to control the workflow of your application by deciding the order in which middleware is queued.

ADR (https://www.youtube.com/watch?v=rlrTyN0aqSk)

-   Action feeds input from HTTP request to a domain layer.
-   Action feeds output from domain layer to a responder.
-   Responder builds the HTTP response headers and body.

# Task 2

-   In progress.
-   Ideally CacheHTTPRequest should only feature in CacheMiddleware.
-   Aware of a bug when submitting empty form (will fix this week).
-   Other approaches could include using memcached or database caching.

# Task 3

-   Completed.

# Task 4

-   In progress.
